# cov_id111

## Hi there 👋️

## About Me:

- 💻️ Working on the [Calmira GNU/Linux-libre](https://gitlab.com/calmiralinux) and [Free Weather Forecat `wrapper`](https://gitlab.com/cov_id111/fwf) projects;
- 🗄️ Developed '[cport](https://gitlab.com/calmiralinux/cabs/cport)' softwar management utility
- 👨‍🏫️ I study programming languages Python and Rust.

## Contact me:

- [Telegram](https://t.me/cov_id111)
- GitLab - this page

## About my projects

### [Calmira GNU/Linux-libre](https://gitlab.com/calmiralinux)

Minimalistic independent source-based libre GNU/Linux distributive.

![](https://gitlab.com/calmiralinux/CalmiraLinux/-/raw/v2.0/pic/screen.png)

### [cport](https://gitlab.com/calmiralinux/cabs/cport)

Ports manager for Calmira GNU/Linux-libre and GNU/Linux distributives builded by
[LFS](https://linuxfromscratch.org). It can install, remove, update and print
information about software in the [port
system](https://gitlab.com/calmiralinux/cabs/Ports).

![](https://gitlab.com/calmiralinux/cabs/cport/-/raw/master/pic/cport_info.gif)
